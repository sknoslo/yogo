import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.utils.Draw;
import entities.Clock;

class WinScene extends Scene
{
    private var time:Float;

    public function new()
    {
        super();

        time = Clock.time;
    }
    override public function begin()
    {
        HXP.screen.scale = 2;
    }

    override public function render()
    {
        super.render();

        var text = "You saved the world in " + Std.int(time / 60 * 100) / 100 + " minutes!";
        Draw.text(text, 10, 16);
        Draw.text("Press ENTER for main menu", 10, 32);
    }
}
