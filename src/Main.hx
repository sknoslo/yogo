import com.haxepunk.Engine;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import entities.Clock;
import entities.Code;

class Main extends Engine
{

    private var mainScene:MainScene;
    private var menuScene:MenuScene;
    private var winScene:WinScene;
    private var loseScene:LoseScene;

    public static var GAME_FINISHED:Bool = false;

    public function new()
    {
        super(800, 480, 60, false);
    }

	override public function init()
	{
#if debug
		HXP.console.enable();
#end

        menuScene = new MenuScene();
		HXP.scene = menuScene;
	}

	public static function main() { new Main(); }

    override public function update()
    {
        if (GAME_FINISHED)
        {
            if (Clock.time < Clock.END_TIME && Code.isCorrect())
            {
                winScene = new WinScene();
                HXP.scene = winScene;
            }
            else
            {
                loseScene = new LoseScene();
                HXP.scene = loseScene;
            }
            
            GAME_FINISHED = false;
            mainScene = new MainScene();
            menuScene.renderResumable = false;
        }

        if (Input.pressed(Key.ENTER) && HXP.scene != mainScene)
        {
            if (HXP.scene == menuScene)
            {
                mainScene = new MainScene();
                HXP.scene = mainScene;

                menuScene.renderResumable = true;
            }
            else
            {
                HXP.scene = menuScene;
            }
        }
        else if (Input.pressed(Key.P) && mainScene.resumable)
        {
            if (HXP.scene == menuScene) HXP.scene = mainScene;
            else HXP.scene = menuScene;
        }

        super.update();
    }
}
