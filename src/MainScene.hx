import com.haxepunk.Scene;
import com.haxepunk.HXP;
import entities.*;
import com.haxepunk.tmx.TmxEntity;
import com.haxepunk.tmx.TmxMap;
import com.haxepunk.debug.Console;

class MainScene extends Scene
{
    private var player:Player;
    private var world:TmxEntity;

    var camX:Float;
    var camY:Float;
    var maxCamOffsetY:Float = 32;

    private var log:Console;

    public var resumable:Bool;

    public function new()
    {
        super();

        log = new Console();
#if debug
        log.enable();
#end
        camera.x = 0 - HXP.halfWidth;
        camera.y = 0 - HXP.halfHeight;

        resumable = false;

        loadWorld();
    }

	public override function begin()
	{
        HXP.screen.scale = 3;
        resumable = true;

        camX = player.centerX;
        camY = player.centerY;
	}

    private function loadWorld()
    {
        Code.genCode();
        var map = TmxMap.loadFromFile("maps/TestMap.tmx");

        var playerLoaded:Bool = false; // if player doesn't load we need to know
        for (entity in map.getObjectGroup("Entities").objects)
        {
            switch (entity.name)
            {
            case "Player":
                playerLoaded = true;
                player = new Player(entity.x, entity.y);
            case "PlayerTest":
            case "Clock":
                add(new Clock(entity.x, entity.y));
            case "Code1":
                add(new CodeSymbol(entity.x, entity.y, 0, false));
            case "Code2":
                add(new CodeSymbol(entity.x, entity.y, 1, false));
            case "Code3":
                add(new CodeSymbol(entity.x, entity.y, 2, false));
            case "Code4":
                add(new CodeSymbol(entity.x, entity.y, 3, false));
            case "Code5":
                add(new CodeSymbol(entity.x, entity.y, 4, false));
            case "Clue1":
                add(new CodeSymbol(entity.x, entity.y, 0, true));
            case "Clue2":
                add(new CodeSymbol(entity.x, entity.y, 1, true));
            case "Clue3":
                add(new CodeSymbol(entity.x, entity.y, 2, true));
            case "Clue4":
                add(new CodeSymbol(entity.x, entity.y, 3, true));
            case "Clue5":
                add(new CodeSymbol(entity.x, entity.y, 4, true));
            case "Detonator":
                add(new Detonator(entity.x, entity.y));
            case "Platform":
                var x1 = entity.x;
                var y1 = entity.y;
                var x2 = Std.parseInt(entity.custom.resolve("ox")) * 16 + x1;
                var y2 = Std.parseInt(entity.custom.resolve("oy")) * 16 + y1;
                add(new Platform(x1, y1, x2, y2));
            default:
                trace("Unknown entity: " + entity.name);
            }
        }

        if (!playerLoaded)
        {
            trace("Player not loaded. Creating default");
            // Create Default Player...
            player = new Player(120*16, 120*16);
        }

        world = new TmxEntity(map);
        world.loadGraphic("graphics/TileSheet.png", ["Foreground"]);
        world.loadMask("Foreground", "walls");
        add(world);
        add(player);
    }

    override public function update()
    {
        super.update();

        camX = player.centerX;

        // Set camera postion. Cast to int to avoid weirdness in flash
        camera.x = Std.int(camX - HXP.halfWidth);

        // Calculate new camera y value
        if (player.centerY - camY  < -maxCamOffsetY)
        {
            camY = player.centerY + maxCamOffsetY;
        }
        else if (player.centerY - camY > maxCamOffsetY)
        {
            camY = player.centerY - maxCamOffsetY;
        }
        camera.y = Std.int(camY - HXP.halfHeight);
    }
}
