import com.haxepunk.Scene;
import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.utils.Draw;
import com.haxepunk.graphics.Spritemap;
import entities.Clock;
import entities.Code;
import com.haxepunk.Sfx;

class LoseScene extends Scene
{
    private var outOfTime:Bool = false;

    private var solutionCopy:Array<Int>;
    private var valueCopy:Array<Int>;
    private var explosion:Spritemap;
    private var exp:Sfx;

    public function new()
    {
        super();

        outOfTime = Clock.time >= Clock.END_TIME;
        solutionCopy = Code.solution.copy();
        valueCopy = Code.value.copy();

        explosion = new Spritemap("graphics/Explosion.png", 200, 120);
        explosion.add("anim", [0, 1, 2, 3], 15, true);
        exp = new Sfx("audio/exp.mp3");

        var e = new Entity(0, 0);
        e.addGraphic(explosion);
        add(e);
        explosion.play("anim");
    }
    override public function begin()
    {
        HXP.screen.scale = 4;
        exp.play(.5);
    }

    override public function render()
    {
        if (exp.playing) super.render();
        else
        {
            HXP.screen.scale = 2;
            var text:String;

            // If we weren't out of time, must be wrong code..
            if (outOfTime) text = "You ran out of time!!";
            else text = "You entered the wrong code!!";

            Draw.text(text, 10, 16);
            Draw.text("Press ENTER for main menu", 10, 32);
        }
    }
}
