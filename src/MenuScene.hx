import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.utils.Draw;
import com.haxepunk.graphics.Image;

class MenuScene extends Scene
{
    public var renderResumable:Bool = false;
    private var instructions:Image;

    public function new()
    {
        super();

        instructions = new Image("graphics/Instructions.png");
        instructions.scale = 2;
    }
    override public function begin()
    {
        HXP.screen.scale = 2;
    }

    override public function render()
    {
        super.render();

        Draw.text("Press ENTER to start new game", 10, 16);
        if (renderResumable) Draw.text("Press P to resume", 10, 32);
        Draw.graphic(instructions, 64, 96);
    }
}
