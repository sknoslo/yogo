package entities;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;

class Player extends Entity
{
    private var sprites:Spritemap;

    private var vx:Float = 0;
    private var vy:Float = 0;

    private var ax:Float = 400;
    private var ay:Float = 300;

    private var jumpVel:Float = 150;

    private var mx:Float = 125; // max x velocity

    private var onGround:Bool = false;
    private var justLanded:Bool = false;

    private var runSound:Sfx;
    private var landSound:Sfx;

    public function new(x:Int, y:Int)
    {
        super(x, y);

        type = "player";

        setHitbox(14, 15, -1, -1);
        sprites = new Spritemap("graphics/PlayerSheet.png", 16, 16);
        sprites.add("idle", [0, 1, 2, 3], 10, true);
        sprites.add("run", [4, 5, 6, 7, 8, 9, 10, 11], 10, true);
        sprites.add("jump", [12, 13], 5, true);
        sprites.add("fall", [14, 15], 5, true);

        graphic = sprites;

        // Player should draw infront of most things
        layer = 1;

        runSound = new Sfx("audio/run.mp3");
        landSound = new Sfx("audio/land.mp3");
    }

    override public function update()
    {
        super.update();

        var dt = HXP.elapsed;

        var dx:Float = 0;
        var dy:Float = 0;

        // Update X movement ///////////////////////
        var right = Input.check(Key.RIGHT);
        var left = Input.check(Key.LEFT);
        if (right && !left && collide("walls", x+1, y) == null) vx += ax * dt;
        else if (left && !right && collide("walls", x-1, y) == null) vx -= ax * dt;
        else vx *= 0.8;

        // set a threshold for stopping
        if (Math.abs(vx) < 1) vx = 0;

        if (vx > mx) vx = mx;
        else if (vx < -mx) vx = -mx;
        ////////////////////////////////////////////

        // Update Y movement ///////////////////////
        onGround = (collide("walls", x, y+1) != null) ||
            (collide("button", x, y+1) != null) ||
            (collide("platform", x, y+8) != null);

        if (onGround)
        {
            if (Input.pressed(Key.UP)) vy = -jumpVel;
        }
        else
        {
            vy += ay * dt;
        }
        ////////////////////////////////////////////


        dx += vx * dt;
        dy += vy * dt;
        moveBy(dx, dy, ["walls", "button"]);

        updateAnimationAndSound();
    }

    private function updateAnimationAndSound()
    {
        if (justLanded)
        {
            justLanded = false;
            landSound.play(.10);
        }

        if (vx < 0) sprites.flipped = true;
        if (vx > 0) sprites.flipped = false;

        if (onGround)
        {
            if (vx != 0)
            {
                if (!runSound.playing) runSound.loop(.25);
                sprites.play("run");
            }
            else
            {
                runSound.stop();
                sprites.play("idle");
            }
        }
        else
        {
            runSound.stop();
            if (vy < 0) sprites.play("jump");
            else sprites.play("fall");
        }
    }

    override public function moveCollideX(e:Entity):Bool
    {
        vx = 0;
        return true;
    }

    override public function moveCollideY(e:Entity):Bool
    {
        justLanded = true;
        vy = 0;
        return true;
    }
}
