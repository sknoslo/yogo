package entities;

import com.haxepunk.graphics.Spritemap;
import com.haxepunk.Entity;

class Detonator extends Entity
{
    private var pressed:Bool = false;
    private var button:Spritemap;

    public function new(x:Int, y:Int)
    {
        super(x, y);

        type = "button";

        setHitbox(32, 10, 0, -6);

        button = new Spritemap("graphics/Button.png", 32, 16);
        button.add("up", [0], 1, false);
        button.add("down", [1], 1, false);

        graphic = button;
    }

    override public function update()
    {
        super.update();

        var player = collide("player", x, y-2);
        if (player != null)
        {
            setHitbox(32, 6, 0, -10);
            button.play("down");
            pressed = true;
        }
        else
        {
            setHitbox(32, 10, 0, -6);
            button.play("up");
            pressed = false;
        }

        if (pressed)
        {
            Main.GAME_FINISHED = true;
        }
    }
}
