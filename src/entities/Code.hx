package entities;

class Code
{
    public static var numSymbols:Int = 5;
    public static var value:Array<Int> = [0, 0, 0, 0, 0];
    public static var solution:Array<Int> = [1, 0, 0, 0, 0];

    public static function genCode()
    {
        for (i in 0...value.length)
        {
            value[i] = Std.random(numSymbols);
        }

        for (i in 0...solution.length)
        {
            solution[i] = Std.random(numSymbols);
        }
    }

    public static function isCorrect():Bool
    {
        return Std.string(value) == Std.string(solution);
    }
}
