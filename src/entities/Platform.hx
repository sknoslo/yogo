package entities;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;

class Platform extends Entity
{
    private var x1:Int;
    private var x2:Int;
    private var y1:Int;
    private var y2:Int;

    public var v:Float = 50;

    private var towardsStart:Bool = false;

    public function new(x1:Int, y1:Int, x2:Int, y2:Int)
    {
        super(x1, y1);
        type = "platform";

        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;

        setHitbox(48, 16);

        graphic = new Image("graphics/Platform.png");
    }

    override public function update()
    {
        var dt = HXP.elapsed;
        super.update();

        var oldX = x;
        if (!towardsStart) moveTowards(x2, y2, v * dt);
        else moveTowards(x1, y1, v * dt);
        var dx = x - oldX;

        if (x == x1 && y == y1) towardsStart = false;
        else if (x == x2 && y == y2) towardsStart = true;

        var e:Entity = collide("player", x, y - 2);
        if (e != null)
        {
            if (e.y < this.y - 2) e.y = this.y - e.height - 1;

            e.x += dx;
        }
    }
}
