package entities;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Image;
import com.haxepunk.Sfx;

class Clock extends Entity
{
    private var clockFrame:Image;
    private var clockHand:Image;

    public static var time:Float = 0;
    public static var END_TIME:Float = 600;

    private var nextTick:Float;
    private var tickFreq:Float;

    private var tick:Sfx;
    public function new(x:Int, y:Int)
    {
        super(x + 24, y + 24);
        time = 0;

        // Push this to the back.
        layer = 100;

        clockFrame = new Image("graphics/Clock.png");
        clockFrame.centerOrigin();
        clockHand = new Image("graphics/ClockHand.png");
        clockHand.centerOrigin();
        addGraphic(clockFrame);
        addGraphic(clockHand);

        tick = new Sfx("audio/tick.mp3");
        nextTick = 1;
        tickFreq = 1;
    }

    override public function update()
    {
        super.update();

        time += HXP.elapsed;

        if (time > 590) tickFreq = .25;
        if (time > nextTick)
        {
            nextTick += tickFreq;
            tick.play(.2);
        }

        if (time >= END_TIME) Main.GAME_FINISHED = true;

        clockHand.angle = -time / END_TIME * 360.0;
    }
}
