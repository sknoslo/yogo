package entities;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.geom.Rectangle;

class CodeSymbol extends Entity
{
    private var index:Int;
    private var symbols:Spritemap;
    private var curSymbol:Int;

    private var solutionSymbol:Bool; // ugly hack.. I'm running out of time though.

    private var playerHover:Bool = false;

    public function new(x:Int, y:Int, index:Int, solutionSymbol:Bool)
    {
        super(x, y);

        this.index = index;
        this.solutionSymbol = solutionSymbol;

        layer = 0;
        symbols = new Spritemap("graphics/Codes.png", 16, 16);
        for (i in 0...Code.numSymbols)
        {
            symbols.add(Std.string(i), [i], 1, false);
        }

        // if its just a solution, only need to be near it.. not on it
        if (solutionSymbol) setHitbox(48, 48, 16, 16);
        else setHitbox(2, 32, -7, 16);
        graphic = symbols;
    }

    private function nextSymbol()
    {
        Code.value[index] += 1;
        Code.value[index] %= Code.numSymbols;
    }

    override public function update()
    {
        super.update();

        playerHover = collide("player", x, y) != null;

        // give visual cue if player is hovering over tile
        if (playerHover)
        {
            symbols.alpha = 1;
            if (solutionSymbol) symbols.color = 0x00FF00;
            else symbols.color = 0xFF0000;
            if (Input.pressed(Key.A) && !solutionSymbol)
            {
                nextSymbol();
            }
        }
        else
        {
            if (solutionSymbol) symbols.alpha = 0;
            symbols.color = 0xFFFFFF;
        }

        if (solutionSymbol) symbols.play(Std.string(Code.solution[index]));
        else symbols.play(Std.string(Code.value[index]));
    }
}
